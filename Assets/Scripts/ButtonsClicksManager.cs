﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum ButtonType { OneClick,AlwaysAcitve}
public class ButtonsClicksManager : MonoBehaviour {
    public ButtonType buttonType;
    public Button button;
    public float disableTime;
	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => HandleButton(disableTime));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public IEnumerator DisableButtonForTime(float time)
    {
        button.interactable = false;
        yield return new WaitForSeconds(time);
        button.interactable = true;
    }
    public void HandleButton(float time)
    {
        StartCoroutine(DisableButtonForTime(time));
    }
}

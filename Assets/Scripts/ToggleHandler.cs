﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class ToggleHandler : MonoBehaviour {
    public UnityEvent toggleOnEvent;
    public UnityEvent toggleOffEvent;
    public Toggle toggle;
    void Start()
    {
        toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener((val) => {
            if (val == true)
                ToggleON();
            else
                ToggleOff();
        });
       // toggle.onValueChanged.AddListener((val) => ToggleOff());
    }
    public void ToggleON()
    {
        //if (toggle.isOn)
       // {
            toggleOnEvent.Invoke();
       // }
    }
    public void ToggleOff()
    {
    //    if (!toggle.isOn)
      //  {
            toggleOffEvent.Invoke();
      //  }
    }
}

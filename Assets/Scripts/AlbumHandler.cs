﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class AlbumHandler : MonoBehaviour {

    private DirectoryInfo imageDirectory;
    private FileInfo[] fileInfo;
    public List<string> imagesList = new List<string>();
    public GameObject newImage;
    public GameObject albumContainer;
    public RawImage PhotoViewer;

    // Use this for initialization
    void Start () {
        ///find the directory and get all of its files then read all textures in it and put them in a list 
        
        imageDirectory = new DirectoryInfo(Application.streamingAssetsPath + "/Album/");
        fileInfo = imageDirectory.GetFiles("*.jpg");
        ReadAllImagesInFolder();


    }
    public void ReadAllImagesInFolder()
    {
        foreach (var item in fileInfo)
        {
            ///Add the file name to the List
            imagesList.Add(item.Name);
        }
        // StartCoroutine(InstallImages());
        InstallImages();
    }
    public void  InstallImages()
    {
        foreach (var image in imagesList)
        {
            Texture2D tex = new Texture2D(200, 200, TextureFormat.ARGB32, false);
            Debug.Log(image);
            byte[] b = File.ReadAllBytes(Application.streamingAssetsPath + "/Album/" + image);
            tex.LoadImage(b);
            tex.Apply();
            newImage.GetComponent<RawImage>().texture = tex;
            GameObject ins= Instantiate(newImage, albumContainer.transform, false);
            ins.GetComponent<Button>().onClick.AddListener(() =>
            {
                ClickOnImage();
            });
        }
    }
    public void ClickOnImage()
    {
        Debug.Log("Clicked");
        PhotoViewer.texture = EventSystem.current.currentSelectedGameObject.GetComponent<RawImage>().texture;
        PhotoViewer.gameObject.SetActive(true);
        PhotoViewer.SetNativeSize();
    }
}
